//
//  ViewController.swift
//  FamiTravl
//
//  Created by Orsan Awwad on 23/05/2016.
//  Copyright © 2016 Orsan Awwad. All rights reserved.
//

import UIKit
import CoreLocation

class StartViewController: UIViewController {

    var manager: CLLocationManager!
    
    @IBOutlet weak var welcomeTitleLabel: UILabel!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var continueButtonOutlet: UIButton!
    @IBAction func continueButton(sender: AnyObject) {
        if(!nameTextField.isEqual("")){
            let name = nameTextField.text
            GlobalMethods.createUser(name!)
            dispatch_async(dispatch_get_main_queue()){
                self.performSegueWithIdentifier("startUpSegue2", sender: self)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        
        welcomeTitleLabel.hidden = true
        nameTextField.hidden = true
        continueButtonOutlet.hidden = true
        
        if(!GlobalMethods.getCurrentUserFromDevice().isEmpty)
        {
            dispatch_async(dispatch_get_main_queue()){
                self.performSegueWithIdentifier("startUpSegue2", sender: self)
            }
        }
        else
        {
            welcomeTitleLabel.hidden = false
            nameTextField.hidden = false
            continueButtonOutlet.hidden = false
        }
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "startUpSegue2")
        {
            print("True")
        }
    }
    
    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        if(identifier == "startUpSegue2")
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    func requestLocation(){
        switch CLLocationManager.authorizationStatus() {
        case .AuthorizedAlways:
            break
        case .NotDetermined:
            manager.requestAlwaysAuthorization()
        case .AuthorizedWhenInUse, .Restricted, .Denied:
            let alertController = UIAlertController(
                title: "Background Location Access Disabled",
                message: "In order for the app to work as intended, please open this app's settings and set location access to 'Always'.",
                preferredStyle: .Alert)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
            alertController.addAction(cancelAction)
            
            let openAction = UIAlertAction(title: "Open Settings", style: .Default) { (action) in
                if let url = NSURL(string:UIApplicationOpenSettingsURLString) {
                    UIApplication.sharedApplication().openURL(url)
                }
            }
            alertController.addAction(openAction)
            
            self.presentViewController(alertController, animated: true, completion: { 
                print("Done")
            })
        }
    }
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
}

