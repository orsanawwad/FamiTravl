//
//  NewGroupViewController.swift
//  FamiTravl
//
//  Created by Orsan Awwad on 28/05/2016.
//  Copyright © 2016 Orsan Awwad. All rights reserved.
//

import UIKit
import GoogleMaps

class NewGroupViewController: UIViewController {

    @IBOutlet weak var newGroupName: UITextField!
    
    @IBOutlet weak var mapPreview: GMSMapView!
    
    @IBOutlet weak var addressPreview: UITextView!
    
    @IBAction func pickDestination(sender: AnyObject) {
        openPlacePicker()
    }
    @IBAction func createNewGroupButton(sender: AnyObject) {
        createNewGroup()
    }
    let placesClient = GMSPlacesClient()
    
    var placePicker: GMSPlacePicker?
    
    var destination: CLLocationCoordinate2D?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround() 
        mapPreview.hidden = true
        addressPreview.hidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpMap(location: CLLocationCoordinate2D, place: GMSPlace){
        let camera = GMSCameraPosition.cameraWithLatitude(location.latitude, longitude: location.longitude, zoom: 16)
        let mapView = GMSMapView.mapWithFrame(CGRectZero, camera: camera)
        
        self.destination = location
        
        self.mapPreview.hidden = false
        self.addressPreview.hidden = false
        
        self.mapPreview.camera = camera
        
        let marker = GMSMarker()
        marker.position = location
        marker.title = "Destination"
        marker.map = self.mapPreview
        
        self.addressPreview.text = place.formattedAddress
        self.addressPreview.editable = false
    }
    
    func createNewGroup(){
        if(self.destination == nil || !CLLocationCoordinate2DIsValid(self.destination!))
        {
            showAlert()
        }
        else if (newGroupName.text == "")
        {
            showAlertNoName()
        }
        else {
            let name = newGroupName.text
            let roomKey = GlobalMethods.createRoom(name!, coordinates: self.destination!)
            if(roomKey != "")
            {
                self.navigationController?.popViewControllerAnimated(true)
            }
            else{
                print("something is not quite right...")
            }
        }
    }
    
    func openPlacePicker(){
        let center = CLLocationCoordinate2DMake((manager.location?.coordinate.latitude)!, (manager.location?.coordinate.longitude)!)
        let northEast = CLLocationCoordinate2DMake(center.latitude + 0.001, center.longitude + 0.001)
        let southWest = CLLocationCoordinate2DMake(center.latitude - 0.001, center.longitude - 0.001)
        let viewport = GMSCoordinateBounds(coordinate: northEast, coordinate: southWest)
        let config = GMSPlacePickerConfig(viewport: viewport)
        placePicker = GMSPlacePicker(config: config)
        
        placePicker!.pickPlaceWithCallback({ (place: GMSPlace?, error: NSError?) -> Void in
            if let error = error {
                print("Pick Place error: \(error.localizedDescription)")
                return
            }
            
            if let place = place {
                self.setUpMap(place.coordinate, place: place)
            } else {
                self.showAlert()
            }
        })
    }
    
    func showAlert(){
        let alert = UIAlertController(title: "No destination selected", message: "You have not selected a destination for this room, you need to select a destination in order to continue", preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: "Pick destination", style: UIAlertActionStyle.Default, handler: {(alert: UIAlertAction!) in self.openPlacePicker()}))
        alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Cancel, handler: {(alert: UIAlertAction!) in print("Dismissed")}))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func showAlertNoName(){
        let alert = UIAlertController(title: "No name!", message: "You have to type a name for this room", preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Cancel, handler: {(alert: UIAlertAction!) in print("Dismissed")}))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
}

/*
placesClient.currentPlaceWithCallback({ (placeLikelihoods, error) -> Void in
    guard error == nil else {
        print("Current Place error: \(error!.localizedDescription)")
        return
    }
    
    if let placeLikelihoods = placeLikelihoods {
        let placeLiklihood = placeLikelihoods.
    }
})
*/
/*
 // MARK: - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
 // Get the new view controller using segue.destinationViewController.
 // Pass the selected object to the new view controller.
 }
 */
