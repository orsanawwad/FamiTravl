//
//  GroupMapViewController.swift
//  FamiTravl
//
//  Created by Orsan Awwad on 31/05/2016.
//  Copyright © 2016 Orsan Awwad. All rights reserved.
//

/*TODO:
 * 1. get all members ids
 * 2. get all members names
 * 3. create markers by members count with random color
 * 4. observe their location
 * 5. update map accordingly
 * 6. add directions?
 * 7. set destination
 */


import UIKit

import Firebase

import GoogleMaps

import CoreLocation

class GroupMapViewController: UIViewController, GMSMapViewDelegate {

    var roomId: String?
    
    var users: [User] = []
    
    var destination: CLLocationCoordinate2D!
    
    var destincationMarker: GMSMarker!
    
    var destRoom: FIRDatabaseReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
        getMembers()
        getDestinationPoint()
    }

    @IBOutlet var mapView: GMSMapView!
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let dest = segue.destinationViewController as! GroupInfoViewController
        dest.roomId = self.roomId
        destRoom.removeAllObservers()
    }
    
    func getDestinationPoint(){
        let roomRef = roomsRef.child(roomId!)
        self.destRoom = roomRef.child("destination")
        destRoom.observeEventType(FIRDataEventType.Value, withBlock: { (snapshot) in
            var dest = CLLocationCoordinate2D()
            dest.latitude = snapshot.value!["latitude"] as! Double
            dest.longitude = snapshot.value!["longitude"] as! Double
            self.destination = dest
            self.destincationMarker = GMSMarker(position: dest)
            self.destincationMarker.icon = GMSMarker.markerImageWithColor(UIColor.whiteColor())
            self.destincationMarker.title = "Destination"
            self.destincationMarker.map = self.mapView
        })
    }
    
    func getMembers(){
        let roomRef = roomsRef.child(roomId!)
        let membersRef = roomRef.child("members")
        membersRef.observeEventType(FIRDataEventType.Value, withBlock: { (snapshot) in
            let enumerator = snapshot.children
            while let rest = enumerator.nextObject() as? FIRDataSnapshot {
                let id = rest.key
                if(!self.checkIfUserExistsInArray(id))
                {
                    var user = User()
                    user.id = id
                    self.users.append(user)
                }
            }
            self.getNames()
            self.observeUsersLocation()
        })
    }
    
    
    func getNames(){
        for user in users
        {
            usersRef.child(user.id).observeSingleEventOfType(.Value, withBlock: { (snapshot) in
                user.name = snapshot.value!["name"] as! String
                let color = self.generateRandomColor()
                user.marker.icon = GMSMarker.markerImageWithColor(color)
            })
        }
    }
    
    
    func generateRandomColor() -> UIColor {
        let hue : CGFloat = CGFloat(arc4random() % 256) / 256 // use 256 to get full range from 0.0 to 1.0
        let saturation : CGFloat = CGFloat(arc4random() % 128) / 256 + 0.5 // from 0.5 to 1.0 to stay away from white
        let brightness : CGFloat = CGFloat(arc4random() % 128) / 256 + 0.5 // from 0.5 to 1.0 to stay away from black
        
        return UIColor(hue: hue, saturation: saturation, brightness: brightness, alpha: 1)
    }
    
    func observeUsersLocation(){
        for user in users
        {
            usersRef.child(user.id).child("location").observeEventType(.Value, withBlock: { (snapshot) in
                var location = CLLocationCoordinate2D()
                location.latitude = snapshot.value!["latitude"] as! Double
                location.longitude = snapshot.value!["longitude"] as! Double
                user.location = location
                user.marker.position = location
                user.marker.title = user.name
                user.marker.map = self.mapView
                self.ShowAllMarkers()
            })
        }
    }
    
    func checkIfUserExistsInArray(id: String) -> Bool{
        for user in users
        {
            if(user.id == id)
            {
                return true
            }
        }
        return false
    }
    
    func ShowAllMarkers() {
        var markers = getMarkersArray()
        if destination != nil
        {
            markers.append(self.destincationMarker)
        }
        var firstLocation: CLLocationCoordinate2D = markers[0].position
        var bounds: GMSCoordinateBounds = GMSCoordinateBounds(coordinate: firstLocation, coordinate: firstLocation)
        for marker in markers {
            bounds = bounds.includingCoordinate(marker.position)
        }
        mapView.animateWithCameraUpdate(GMSCameraUpdate.fitBounds(bounds, withPadding: 50.0))
    }

    func getMarkersArray() -> [GMSMarker] {
        var markers = [GMSMarker]()
        for user in users
        {
            markers.append(user.marker)
        }
        return markers
    }
}
