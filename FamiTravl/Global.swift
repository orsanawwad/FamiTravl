//
//  Global.swift
//  FamiTravl
//
//  Created by Orsan Awwad on 27/05/2016.
//  Copyright © 2016 Orsan Awwad. All rights reserved.
//

import Foundation
import Firebase
import CoreLocation


let preferences = NSUserDefaults.standardUserDefaults()

let currentUserIdPreference = "currentUserId"

let rootRef = FIRDatabase.database().reference()

let usersRef = rootRef.child("users")

let roomsRef = rootRef.child("rooms")

let manager = CLLocationManager()

class GlobalConstants{
    
}

class GlobalVariables{
    var userId: String?
    var roomId: String?
    var name: String?
}

class GlobalMethods: MethodsProtocol{
    
    static func userExistsOnDevice() -> Bool {
        if preferences.objectForKey(currentUserIdPreference) == nil {
            return false
        } else {
            return true
        }
    }
    
    static func getCurrentUserFromDevice() -> String {
        if(userExistsOnDevice())
        {
            return preferences.objectForKey(currentUserIdPreference) as! String
        }
        else
        {
            return ""
        }
    }
    
    static func createUser(name: String) -> Bool {
        if(!userExistsOnDevice())
        {
            let signupUser = usersRef.childByAutoId()
            signupUser.setValue(["name":name,"location":["latitude":0.0,"longitude":0.0]])
            saveUserToDevice(signupUser.key)
            return true
        }
        else
        {
            return false
        }
    }
    
    static func saveUserToDevice(userId: String) -> Bool {
        if(!userExistsOnDevice())
        {
            preferences.setObject(userId, forKey: currentUserIdPreference)
            return preferences.synchronize()
        } else {
            return false
        }
    }
    
    static func createRoom(name: String, coordinates: CLLocationCoordinate2D) -> String {
        if(userExistsOnDevice())
        {
            let newRoom = roomsRef.childByAutoId()
            newRoom.setValue(["name":name,"destination":["latitude":coordinates.latitude,"longitude":coordinates.longitude],"admin":getCurrentUserFromDevice()])
            newRoom.child("members").updateChildValues([getCurrentUserFromDevice():true])
            let currentUser = usersRef.child(getCurrentUserFromDevice())
            currentUser.child("rooms").updateChildValues([newRoom.key:true])
            return newRoom.key
        }
        else
        {
            return ""
        }
    }
}

protocol MethodsProtocol{
    
    static func userExistsOnDevice() -> Bool
    
    static func getCurrentUserFromDevice() -> String
    
    static func saveUserToDevice(userId: String) -> Bool
    
    static func createUser(name: String) -> Bool
    
//    static func renameUser(userId: String, name: String) -> Bool
//    
//    static func getUser(userId: String) -> String
//    
    static func createRoom(name: String, coordinates: CLLocationCoordinate2D) -> String
//
//    static func joinRoom(userId: String, roomId: String) -> Bool

}