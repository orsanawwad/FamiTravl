//
//  GroupInfoViewController.swift
//  FamiTravl
//
//  Created by Orsan Awwad on 31/05/2016.
//  Copyright © 2016 Orsan Awwad. All rights reserved.
//

import UIKit
import Firebase
class GroupInfoViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var roomId: String?
    
    var membersId: [String] = []
    
    var memberAdmin: String?
    
    var membersName: [String] = []
    
    @IBOutlet weak var membersTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        membersTableView.delegate = self
        membersTableView.dataSource = self
        grabMembers()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let dest = segue.destinationViewController as! GroupCodeViewController
        dest.roomId = self.roomId
    }

    func grabMembers(){
        let roomMembers = roomsRef.child(roomId!).child("members")
        roomMembers.observeSingleEventOfType(.Value, withBlock: { (snapshot) in
            let enumerator = snapshot.children
            while let rest = enumerator.nextObject() as? FIRDataSnapshot {
                let id = rest.key 
                self.membersId.append(id)
            }
            self.grabTheirNames()
            }) { (error) in
                print(error.localizedDescription)
        }
    }
    
    func grabTheirNames(){
        for id in membersId
        {
            let user = usersRef.child(id)
            user.observeSingleEventOfType(FIRDataEventType.Value, withBlock: { (snapshot) in
                let name = snapshot.value!["name"] as! String
                self.membersName.append(name)
                self.membersTableView.reloadData()
                }, withCancelBlock: { (error) in
                    print(error.localizedDescription)
            })
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return membersId.count
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Members"
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("groupMembersCellIdentifier", forIndexPath: indexPath) as! UITableViewCell
        
        if(membersName.count == membersId.count){
            cell.textLabel!.text = membersName[indexPath.row]
            let isAdmin = roomsRef.childByAppendingPath(roomId!).child("admin")
            isAdmin.observeSingleEventOfType(FIRDataEventType.Value, withBlock: { (snapshot) in
                if(snapshot.value as! String == self.membersId[indexPath.row])
                {
                    cell.detailTextLabel!.text = "Admin"
                }
                else{
                    cell.detailTextLabel!.text = ""
                }
                }, withCancelBlock: { (error) in
                    print(error.localizedDescription)
            })
        }
        return cell
    }
    
}
