//
//  GroupNamesTableViewCell.swift
//  FamiTravl
//
//  Created by Orsan Awwad on 30/05/2016.
//  Copyright © 2016 Orsan Awwad. All rights reserved.
//

import UIKit

class GroupNamesTableViewCell: UITableViewCell {

    @IBOutlet weak var groupNameLabel: UILabel!
    @IBOutlet weak var isAdminLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    var roomId: String?
    var name: String?
    var admin: String?
    
    func config(roomId: String){
        
        self.roomId = roomId
        
        let theRoom = roomsRef.child(roomId).observeSingleEventOfType(.Value, withBlock: { (snapshot) in
            
            self.name = snapshot.value!["name"] as! String
         
            self.groupNameLabel.text = self.name
            
            self.admin = snapshot.value!["admin"] as! String
            
            if(self.admin == GlobalMethods.getCurrentUserFromDevice())
            {
                self.isAdminLabel.text = "Admin"
            }
            else
            {
                self.isAdminLabel.text = ""
            }
            
            }) { (error) in
                print(error.localizedDescription)
        }
    }
    


    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
