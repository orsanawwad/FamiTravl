//
//  GroupCodeViewController.swift
//  FamiTravl
//
//  Created by Orsan Awwad on 31/05/2016.
//  Copyright © 2016 Orsan Awwad. All rights reserved.
//

import UIKit
import QRCode
import Firebase

class GroupCodeViewController: UIViewController {

    var roomId: String?
    
    @IBOutlet weak var qrCodeForRoom: UIImageView!
    
    @IBOutlet weak var roomIdLabel: UILabel!
    @IBAction func exitRoom(sender: AnyObject) {
        exitRoom()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var qrCode = QRCode(roomId!)
        qrCodeForRoom.image = qrCode?.image
        roomIdLabel.text = roomId
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func generateQRCodeFromString(string: String) -> UIImage? {
        let data = string.dataUsingEncoding(NSISOLatin1StringEncoding)
        
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            filter.setValue("H", forKey: "inputCorrectionLevel")
            let transform = CGAffineTransformMakeScale(10, 10)
            
            if let output = filter.outputImage?.imageByApplyingTransform(transform) {
                return UIImage(CIImage: output)
            }
        }
        
        return nil
    }

    func exitRoom() {
        let roomRef = roomsRef.child(roomId!)
        roomRef.observeSingleEventOfType(.Value, withBlock: { (snapshot) in
            let admin = snapshot.value!["admin"] as! String
            
            let members = roomRef.child("members")
            let exactMember = members.child(GlobalMethods.getCurrentUserFromDevice())
            exactMember.removeValue()
            
            let exactGroupFromUser = usersRef.child(GlobalMethods.getCurrentUserFromDevice()).child("rooms").child(self.roomId!)
            exactGroupFromUser.removeValue()
            
            if(admin == GlobalMethods.getCurrentUserFromDevice())
            {
                self.setAnotherAdminForRoom()
            }
            self.navigationController?.popToRootViewControllerAnimated(true)
        })
    }

    func setAnotherAdminForRoom(){
        let roomRef = roomsRef.child(roomId!)
        roomRef.observeSingleEventOfType(.Value, withBlock: { (snapshot) in
            if(snapshot.hasChild("members"))
            {
                var count = 0
                let enumerator = snapshot.children
                while let rest = enumerator.nextObject() as? FIRDataSnapshot {
                    if(count == 1)
                    {
                        break
                    }
                    roomRef.setValue(["admin":rest.key])
                    count = count + 1
                }
            }
            else
            {
                roomRef.removeValue()
            }
        })
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
