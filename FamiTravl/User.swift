//
//  User.swift
//  FamiTravl
//
//  Created by Orsan Awwad on 28/05/2016.
//  Copyright © 2016 Orsan Awwad. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import GoogleMaps

class User {
    var id: String = ""
    var name: String = ""
    var location = CLLocationCoordinate2D()
    var marker = GMSMarker()
}
