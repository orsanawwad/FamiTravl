//
//  GroupsViewController.swift
//  FamiTravl
//
//  Created by Orsan Awwad on 27/05/2016.
//  Copyright © 2016 Orsan Awwad. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import Firebase
import AVFoundation
import QRCodeReader

class GroupsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate, QRCodeReaderViewControllerDelegate {

    
    var rooms: [GroupNamesTableViewCell] = []
    
    var roomsIds: [String] = []
    
    @IBOutlet weak var groupsTableView: UITableView!
    
    var locationManager: CLLocationManager!
    
    lazy var reader: QRCodeReaderViewController = {
        let builder = QRCodeViewControllerBuilder { builder in
            builder.reader          = QRCodeReader(metadataObjectTypes: [AVMetadataObjectTypeQRCode])
            builder.showTorchButton = true
        }
        
        return QRCodeReaderViewController(builder: builder)
    }()
    
    @IBAction func joinNewGroupButton(sender: AnyObject) {
        startJoinGroup()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        locationSetup()
        
        groupsTableView.delegate = self
        groupsTableView.dataSource = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return roomsIds.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = groupsTableView.dequeueReusableCellWithIdentifier("groupNameCellIdentifier", forIndexPath: indexPath) as! GroupNamesTableViewCell
        cell.config(roomsIds[indexPath.row])
        self.rooms.append(cell)
        return cell
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Rooms"
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = rooms[indexPath.row]
        
        let view = GroupMapViewController()
        
        self.performSegueWithIdentifier("fromGroupsViewToGroupMapView", sender: cell.roomId)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "fromGroupsViewToGroupMapView")
        {
            var dest = segue.destinationViewController as! GroupMapViewController
            var id = sender as! String
            dest.roomId = id
        }
    }
    
    func locationSetup(){
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if(GlobalMethods.userExistsOnDevice())
        {
            let userUpdate = usersRef.child(GlobalMethods.getCurrentUserFromDevice()).child("location").setValue(["latitude":locations[0].coordinate.latitude,"longitude":locations[0].coordinate.longitude])
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        getGroups()
    }
    
    func getGroups(){
        
        roomsIds = []
        rooms = []
        
        usersRef.child(GlobalMethods.getCurrentUserFromDevice()).child("rooms").observeSingleEventOfType(.Value, withBlock: { (snapshot) in
            
            if(snapshot.exists())
            {
                let enumerator = snapshot.children
                while let rest = enumerator.nextObject() as? FIRDataSnapshot {
                    print(rest.key)
                    self.roomsIds.append(rest.key)
                }
            }
            self.groupsTableView.reloadData()
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    func startJoinGroup(){
        let alertController = UIAlertController(title: "Enter room id", message: "Enter the room id you want to join", preferredStyle: .Alert)
        let joinRoom = UIAlertAction(title: "Join", style: .Default) { (_) in
            let joinTextField = alertController.textFields![0] as UITextField
            self.joinRoomWithId(joinTextField.text!)
        }
        joinRoom.enabled = false
        
        let launchScanAction = UIAlertAction(title: "Scan", style: .Default) { (_) in
            self.startScan()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (_) in }
        
        alertController.addTextFieldWithConfigurationHandler { (textField) in
            textField.placeholder = "Login"
            
            NSNotificationCenter.defaultCenter().addObserverForName(UITextFieldTextDidChangeNotification, object: textField, queue: NSOperationQueue.mainQueue()) { (notification) in
                joinRoom.enabled = textField.text != ""
            }
        }
        
        alertController.addAction(joinRoom)
        alertController.addAction(launchScanAction)
        alertController.addAction(cancelAction)
        self.presentViewController(alertController, animated: true) {
        }
    }
    
    func startScan(){
        if QRCodeReader.supportsMetadataObjectTypes() {
            reader.modalPresentationStyle = .FormSheet
            reader.delegate               = self
            
            reader.completionBlock = { (result: QRCodeReaderResult?) in
                if let result = result {
                    print("Completion with result: \(result.value) of type \(result.metadataType)")
                }
            }
            
            presentViewController(reader, animated: true, completion: nil)
        }
        else {
            let alert = UIAlertController(title: "Error", message: "Reader not supported by the current device", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: nil))
            
            presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    func reader(reader: QRCodeReaderViewController, didScanResult result: QRCodeReaderResult) {
        self.dismissViewControllerAnimated(true, completion: { [weak self] in
            self!.joinRoomWithId(result.value)
            })
    }
    
    func readerDidCancel(reader: QRCodeReaderViewController) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    

    
    func joinRoomWithId(roomId: String){
        roomsRef.observeSingleEventOfType(FIRDataEventType.Value, withBlock: { (snapshot) in
            if(snapshot.hasChild(roomId))
            {
                usersRef.child(GlobalMethods.getCurrentUserFromDevice()).child("rooms").observeSingleEventOfType(.Value, withBlock: { (snapshot) in
                    if(!snapshot.hasChild(roomId))
                    {
                        usersRef.child(GlobalMethods.getCurrentUserFromDevice()).child("rooms").updateChildValues([roomId:true])
                        roomsRef.child(roomId).child("members").updateChildValues([GlobalMethods.getCurrentUserFromDevice():true])
                        self.getGroups()
                        let alert = UIAlertController(title: "Success", message: "You have successfully joined the room", preferredStyle: .Alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
                        self.presentViewController(alert, animated: true, completion: nil)
                    }
                    else
                    {
                        let alert = UIAlertController(title: "Already joined", message: "You already joined this room", preferredStyle: .Alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
                        self.presentViewController(alert, animated: true, completion: nil)
                    }
                })
            }
            else
            {
                let alert = UIAlertController(title: "Room does not exists", message: "Could not find the specified room, make sure you entered it correctly", preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
            }
            }) { (error) in
                print(error.localizedDescription)
        }
    }
    
}
